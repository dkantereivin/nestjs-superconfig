import {DynamicModule, Module} from '@nestjs/common';
import {ConfigModuleOptions} from '@/types/config-module-options';
import {CONFIG_OPTIONS} from '@/constants';
import {DotenvConfigProvider} from '@/providers/dotenv/dotenv.config-provider';
import {Injectable} from '@nestjs/common/interfaces';
import {EnvironmentConfigProvider} from '@/providers/environment/environment.config-provider';

@Module({})
export class ConfigModule {
    public static register(options: ConfigModuleOptions): DynamicModule {
        return {
            module: ConfigModule,
            providers: [
                {
                    provide: CONFIG_OPTIONS,
                    useValue: options
                },
                ...options.providers.map(pair => pair.provider)
            ],
            exports: [
                ...options.providers.map(pair => pair.provider)
            ]
        }
    }
}
