import {ConfigProvider, ProviderOptionsPair} from '@/providers/config-provider.interface';
import {ConfigProviderOptions} from '@/providers/config-provider-options.interface';

export class ConfigModuleOptions {
    environment: string | (() => string) = 'NODE_ENV';

    providers: ProviderOptionsPair[] = [];
}