import {ConfigModuleOptions} from '@/types/config-module-options';
import {ConfigProvider} from '@/providers/config-provider.interface';
import {ConfigProviderOptions} from '@/providers/config-provider-options.interface';

export function getProviderOptions<OptionsType extends ConfigProviderOptions>(
    moduleOptions: ConfigModuleOptions,
    provider: new (...args: any) => ConfigProvider
): OptionsType {
    return moduleOptions.providers
        .find(pair => pair.provider === provider)!
        .options as OptionsType;
}