import {Injectable} from '@nestjs/common';
import {ConfigProvider} from '@/providers/config-provider.interface';

@Injectable()
export class EnvironmentConfigProvider implements ConfigProvider {
    initialize(): void | Promise<void> {}

    get(key: string): unknown {
        return process.env[key];
    }

    set(key: string, value: string): void | Promise<void> {
        process.env[key] = value;
    }

    keys(): string[] {
        return Object.keys(process.env);
    }
}