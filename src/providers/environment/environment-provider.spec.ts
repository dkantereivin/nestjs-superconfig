import {TestingModule} from '@nestjs/testing/testing-module';
import {Test} from '@nestjs/testing';
import {ConfigModule} from '@/config.module';
import {EnvironmentConfigProvider} from '@/providers/environment/environment.config-provider';

describe('environment-provider', () => {
    let moduleRef: TestingModule,
        service: EnvironmentConfigProvider;

    beforeAll(async () => {
        process.env.NODE_ENV = 'test';
        process.env.SOME_VARIABLE = 'SOME_VALUE';


        moduleRef = await Test.createTestingModule({
            imports: [
                ConfigModule.register({
                    environment: 'NODE_ENV',
                    providers: [
                        {
                            provider: EnvironmentConfigProvider,
                            options: { }
                        }
                    ]
                })
            ]
        }).compile();

        service = moduleRef.get(EnvironmentConfigProvider);
        await service.initialize();
    });

    // explanation: this is to ensure that it's generally reading all process.env vars,
    // as opposed to any sort of selective behaviour
    it('should have more keys than I can be bothered to count', () => {
        expect(service.keys().length).toBeGreaterThan(10);
    });

    it('should get correct key values', () => {
        expect(service.get('NODE_ENV')).toBe('test');
        expect(service.get('SOME_VARIABLE')).toBe('SOME_VALUE');
    });

    it('should set key values and modify process.env', () => {
        service.set('TEST', 'RESULT')
        expect(service.get('TEST')).toBe('RESULT');
        expect(process.env.TEST).toBe('RESULT');
    })
})