import {ConfigProvider} from '@/providers/config-provider.interface';
import {parse} from 'dotenv';
import {readFile} from 'fs/promises';
import {Inject, Injectable} from '@nestjs/common';
import {CONFIG_OPTIONS} from '@/constants';
import {ConfigModuleOptions} from '@/types/config-module-options';
import {DotenvProviderOptions} from '@/providers/dotenv/dotenv.provider-options';
import {getProviderOptions} from '@/utils/getProviderOptions';

@Injectable()
export class DotenvConfigProvider implements ConfigProvider {
    private config!: Record<string, string>;
    private providerOptions: DotenvProviderOptions;

    constructor(
        @Inject(CONFIG_OPTIONS) private moduleOptions: ConfigModuleOptions,
    ) {
        this.providerOptions = getProviderOptions<DotenvProviderOptions>(
            moduleOptions, DotenvConfigProvider
        ) ?? new DotenvProviderOptions();
    }

    async initialize(): Promise<void> {
        const contents = await readFile(
            this.providerOptions.filepath
        );
        this.config = parse(contents) || {};
    }

    get(key: string): unknown {
        return this.config[key];
    }

    keys(): string[] {
        return Object.keys(this.config);
    }
}