import {Test} from '@nestjs/testing';
import {ConfigModule} from '@/config.module';
import {DotenvConfigProvider} from '@/providers/dotenv/dotenv.config-provider';
import {TestingModule} from '@nestjs/testing/testing-module';
import {readFile} from 'fs/promises';
import {join} from 'path';
import {mocked} from 'ts-jest/utils';
import Dict = NodeJS.Dict;

const ENV_CONTENTS = `NODE_ENV=test
VARIABLE_ONE=1
`;

jest.mock('fs/promises');

describe('dotenv-provider', () => {
    let moduleRef: TestingModule,
        service: DotenvConfigProvider,
        envSnapshot: Dict<string>;

    const ENV_DIR = join(process.cwd(), 'temp', 'jest', '.env');

    const fileReader = mocked(readFile, true);
    fileReader.mockResolvedValue(ENV_CONTENTS);

    beforeAll(async () => {
        envSnapshot = {...process.env};

        moduleRef = await Test.createTestingModule({
            imports: [
                ConfigModule.register({
                    environment: 'NODE_ENV',
                    providers: [
                        {
                            provider: DotenvConfigProvider,
                            options: { filepath: ENV_DIR }
                        }
                    ]
                })
            ]
        }).compile();

        service = moduleRef.get(DotenvConfigProvider);
        await service.initialize();
    });

    it('should call fs.readFile with the correct path', async () => {
        await service.initialize();
        expect(fileReader.mock.calls.length).toBe(1);
        expect(fileReader.mock.calls.shift()?.shift()).toBe(ENV_DIR);
    });

    it('should have correct property keys', () => {
        expect(service.keys() === ['NODE_ENV', 'VARIABLE_ONE']);
    });

    it('should get correct key values', () => {
        expect(service.get('NODE_ENV')).toBe('test');
        expect(service.get('VARIABLE_ONE')).toBe('1');
    })

    it('should not modify process.env', async () => {
        await service.initialize();
        expect(process.env).toMatchObject(envSnapshot);
    })
})