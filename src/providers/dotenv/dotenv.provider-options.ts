import {ConfigProviderOptions} from '@/providers/config-provider-options.interface';
import { join } from 'path';

export class DotenvProviderOptions implements ConfigProviderOptions {
    filepath = join(process.cwd(), '.env');
}