import {ConfigProviderOptions} from '@/providers/config-provider-options.interface';

export interface ConfigProvider {
    initialize(): void | Promise<void>;

    keys(): string[];

    get(key: string): unknown;

    set?(key: string, value: unknown): void | Promise<void>;
}

export type ProviderOptionsPair = {
    provider: new (...args: any) => ConfigProvider;
    options: ConfigProviderOptions;
}