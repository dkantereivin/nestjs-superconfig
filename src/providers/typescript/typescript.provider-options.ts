import {ConfigProviderOptions} from '@/providers/config-provider-options.interface';

export class TypescriptProviderOptions implements ConfigProviderOptions {
    data: Record<string, unknown> = {};
}