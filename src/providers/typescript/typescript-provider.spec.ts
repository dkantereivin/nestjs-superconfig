import {TestingModule} from '@nestjs/testing/testing-module';
import {Test} from '@nestjs/testing';
import {ConfigModule} from '@/config.module';
import {TypescriptConfigProvider} from '@/providers/typescript/typescript.config-provider';

const data: Record<string, unknown> = {};

describe('typescript-provider', () => {
    let moduleRef: TestingModule,
        service: TypescriptConfigProvider;

    beforeAll(async () => {
        moduleRef = await Test.createTestingModule({
            imports: [
                ConfigModule.register({
                    environment: 'NODE_ENV',
                    providers: [
                        {
                            provider: TypescriptConfigProvider,
                            options: { data }
                        }
                    ]
                })
            ]
        }).compile();

        service = moduleRef.get(TypescriptConfigProvider);
        await service.initialize();
    });

    it('should start with no keys',  () => {
        expect(service.keys().length < 1);
    });

    it('should update on data reference change and read correctly', () => {
        expect(service.get('TEST_VALUE_GET')).toBeUndefined();
        data.TEST_VALUE_GET = 1;
        expect(service.get('TEST_VALUE_GET')).toBe(1);
    });

    it('should set properties', () => {
        service.set('TEST_VALUE_SET', 1);
        expect(service.get('TEST_VALUE_SET')).toBe(1);
    });

    it('should share a reference correctly', () => {
        const ref = {};
        service.set('TEST_VALUE_REF', ref);
        expect(service.get('TEST_VALUE_REF')).toStrictEqual(ref);
        expect(data.TEST_VALUE_REF).toStrictEqual(ref);
    });
})