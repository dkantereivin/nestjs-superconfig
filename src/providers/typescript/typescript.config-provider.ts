import {Inject, Injectable} from '@nestjs/common';
import {ConfigProvider} from '@/providers/config-provider.interface';
import {CONFIG_OPTIONS} from '@/constants';
import {ConfigModuleOptions} from '@/types/config-module-options';
import {getProviderOptions} from '@/utils/getProviderOptions';
import {TypescriptProviderOptions} from '@/providers/typescript/typescript.provider-options';

@Injectable()
export class TypescriptConfigProvider implements ConfigProvider {
    private providerOptions: TypescriptProviderOptions;

    constructor(
        @Inject(CONFIG_OPTIONS) private moduleOptions: ConfigModuleOptions,
    ) {
        this.providerOptions = getProviderOptions<TypescriptProviderOptions>(
            moduleOptions, TypescriptConfigProvider
        ) ?? {data: {}};
    }

    initialize(): void | Promise<void> {
    }


    get(key: string): unknown {
        const property = this.providerOptions.data[key];
        if (typeof property === 'function') {
            return property();
        }
        return property;
    }

    set(key: string, value: unknown): void {
        this.providerOptions.data[key] = value;
    }

    keys(): string[] {
        return Object.keys(this.providerOptions.data);
    }
}